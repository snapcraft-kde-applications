NOTICE: This repository is depreciated. Please use application upstream repositories to work on snaps.

To get started the documentation can be found 
[here](https://invent.kde.org/teams/neon/-/wikis/snaps)
